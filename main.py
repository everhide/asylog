from aiohttp import web

from app import create_app
from app.settings import HOST, PORT

application = create_app()


if __name__ == '__main__':
    web.run_app(app=application, host=HOST, port=PORT)
