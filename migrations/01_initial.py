from pathlib import Path
import sys

sys.path.append(Path(__file__).absolute().parent.parent.as_posix())

from faker import Faker
from passlib.hash import pbkdf2_sha256 as sha

from app.user.models import User, Permission
from app.blog.models import Post
from app.settings import database


fake = Faker()

database.connect()

User.create_table()
Permission.create_table()
Post.create_table()

User.get_or_create(
    id=1,
    login='admin',
    passwd=sha.encrypt("admin"),
    is_superuser=True,
)

User.get_or_create(
    id=2,
    login='user',
    passwd=sha.encrypt("user"),
    is_superuser=False,
)


Permission.get_or_create(
    id=1,
    user=1,
    group='admin'
)

Permission.get_or_create(
    id=2,
    user=2,
    group='user'
)


for _ in range(50):
    Post.get_or_create(
        title='The %s %s' % (fake.word(), fake.word()),
        content=''.join(['<p>'+fake.text()+'</p>' for _ in range(10)]),
        author=1
    )

database.close()
