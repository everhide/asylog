from peewee_async import PooledPostgresqlDatabase, Manager


DEBUG = True

HOST = '127.0.0.1'
PORT = 8000

PG_NAME = 'asylog'
PG_USER = 'postgres'
PG_PASS = 'postgres'

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

database = PooledPostgresqlDatabase(host=HOST, database=PG_NAME, user=PG_USER, password=PG_PASS)
manager = Manager(database)
