from aiohttp import web
from aiohttp_jinja2 import template, render_template
from aiohttp_security import remember, forget, check_authorized

from app.auth import check_credentials


@template('login.html')
async def login(request):
    return {"title": "Login"}


async def login_verify(request):

    form = await request.post()

    form_login = form.get('login')
    form_password = form.get('password')

    if await check_credentials(form_login, form_password):
        await remember(request, None, form_login)
        return web.HTTPFound('/')
    else:
        return render_template(
            'login.html',
            request,
            context={
                'title': 'Login',
                'message': 'Login or password incorrect'
            }
        )


async def logout(request):
    await check_authorized(request)
    await forget(request, None)
    return web.HTTPFound('/')
