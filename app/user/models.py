import peewee

from app.settings import database


class User(peewee.Model):
    login = peewee.CharField(max_length=20, null=False)
    passwd = peewee.CharField(null=False)
    is_superuser = peewee.BooleanField(null=False, default=False)
    active = peewee.BooleanField(null=False, default=True)

    class Meta:
        db_table = 'user'
        database = database


class Permission(peewee.Model):
    user = peewee.ForeignKeyField(User)
    group = peewee.CharField(null=False)

    class Meta:
        db_table = 'permission'
        database = database
