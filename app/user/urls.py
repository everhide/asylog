from aiohttp import web

from app.user.views import login, login_verify, logout


urls = [
    web.get('/auth/login/', login, name='login'),
    web.post('/auth/login/', login_verify, name='login-verify'),
    web.get('/auth/logout/', logout, name='logout'),
]
