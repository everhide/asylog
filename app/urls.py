from app.user.urls import urls as user_urls
from app.blog.urls import urls as blog_urls


def add_urls(app):
    app.router.add_routes(user_urls)
    app.router.add_routes(blog_urls)
    app.router.add_static('/static/', 'static')
