from aiohttp import web
from aiohttp_jinja2 import setup as add_jinja2
from aiohttp_session import setup as add_sessions
from aiohttp_session.redis_storage import RedisStorage
from aiohttp_security import setup as add_security
from aiohttp_security import SessionIdentityPolicy

from aioredis import create_pool as create_redis_pool
from aioreloader import start as run_aioreloader
from jinja2 import FileSystemLoader

from app.auth import AuthorizationPolicy
from app.urls import add_urls
from app.settings import *


async def create_app():
    redis_pool = await create_redis_pool((REDIS_HOST, REDIS_PORT))
    app = web.Application()

    if DEBUG:
        run_aioreloader()

    app.db_engine = database

    add_sessions(app, RedisStorage(redis_pool))
    add_security(app, SessionIdentityPolicy(), AuthorizationPolicy(database))
    add_jinja2(app, loader=FileSystemLoader('templates'))
    add_urls(app)

    app.on_cleanup.append(db_close_pool)
    return app


async def db_close_pool(app):
    database.close()
