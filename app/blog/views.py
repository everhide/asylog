from aiohttp import web
from aiohttp_jinja2 import template, render_template
from aiohttp_security import authorized_userid, check_permission

from app.settings import manager
from app.blog.models import Post


@template('home.html')
async def home(request):
    context = {
        'title': 'Home',
        'user': await authorized_userid(request),
    }

    newest_posts = await manager.execute(Post.select().limit(5).order_by(Post.id.desc()))
    context['posts'] = newest_posts
    return context


@template('post-list.html')
async def post_list(request):
    context = {
        'title': 'Posts',
        'user': await authorized_userid(request),
    }

    posts = await manager.execute(Post.select().order_by(Post.id.desc()))
    context['posts'] = posts

    return context


@template('post-detail.html')
async def post_detail(request):
    context = {
        'user': await authorized_userid(request),
    }

    post_id = request.match_info['id']
    try:
        post = await manager.get(Post, id=post_id)
        context['title'] = post.title
        context['post'] = post
    except Post.DoesNotExist:
        raise web.HTTPNotFound()

    return context


@template('about.html')
async def about(request):
    context = {
        'title': 'About',
        'user': await authorized_userid(request),
    }
    return context


@template('create.html')
async def create(request):
    # only admins can create posts
    await check_permission(request, 'admin')

    context = {
        'title': 'Create post',
        'user': await authorized_userid(request),
    }
    return context


async def create_post(request):
    # only admins can create posts
    await check_permission(request, 'admin')

    form = await request.post()

    form_title = form.get('title')
    form_content = form.get('content')

    if form_title and form_content:
        Post.create(
            title=form_title,
            content=form_content,
            author=await authorized_userid(request)
        )
        return web.HTTPFound('/posts/')
    else:
        return render_template(
            'create.html',
            request,
            context={
                'title': 'Create',
                'message': 'Invalid form'
            }
        )
