import datetime

import peewee

from app.settings import database, manager
from app.user.models import User


class Post(peewee.Model):
    id = peewee.PrimaryKeyField()
    title = peewee.CharField(max_length=120, null=False)
    content = peewee.TextField()
    created = peewee.DateField(default=datetime.date.today)
    published = peewee.BooleanField(default=True)
    author = peewee.ForeignKeyField(User)

    class Meta:
        db_table = 'post'
        database = database
