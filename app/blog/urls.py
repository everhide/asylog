from aiohttp import web

from app.blog.views import home, post_detail, post_list, about, create, create_post


urls = [
    web.get('/', home, name='home'),
    web.get('/post/{id}/', post_detail, name='post_detail'),
    web.get('/posts/', post_list, name='post_list'),
    web.get('/about/', about, name='about'),
    web.get('/create/', create, name='create'),
    web.post('/create/', create_post, name='create_post'),
]
