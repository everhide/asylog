from typing import Union

from aiohttp_security.abc import AbstractAuthorizationPolicy
from passlib.hash import pbkdf2_sha256

from app.user.models import User, Permission
from app.settings import manager


class AuthorizationPolicy(AbstractAuthorizationPolicy):

    def __init__(self, dbengine):
        self.dbengine = dbengine

    async def authorized_userid(self, identity: str) -> Union[User, bool]:
        try:
            return await manager.get(
                User.select().where(User.login == identity, User.active)
            )
        except User.DoesNotExist:
            return False

    async def permits(self, identity: str, permission: str, context=None) -> bool:

        user = await self.authorized_userid(identity)

        if not user:
            return False

        if user.is_superuser:
            return True

        try:
            has_permission = await manager.get(
                Permission.select().where(
                    Permission.user == user, Permission.group == permission
                )
            )
            return True if has_permission else False
        except Permission.DoesNotExist:
            return False


async def check_credentials(username: str, password: str) -> bool:
    try:
        user = await manager.get(User, login=username)
        is_verified = pbkdf2_sha256.verify(password, user.passwd)
        return True if is_verified else False
    except User.DoesNotExist:
        return False
